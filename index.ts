import { ProfileCuttingStockProblemSolver } from "./ProfileCuttingStockProblemSolver/ProfileCuttingStockProblemSolver";
import { getTestProblem2 } from "./testProblems/testProblem2";
import { getTestProblem1 } from "./testProblems/testProblem1";
import { analyseSolution } from "./utilities/analyseSolution";

// Run test via : node -r ts-node/register index.ts

// const profileCutoffProblem = getTestProblem1();
// const additionalProfilesLength = undefined;

const profileCutoffProblem = getTestProblem2(27, 0.5, [516, 1428, 4281]);

const optimalSolution = ProfileCuttingStockProblemSolver.solveCuttingStockProblem(profileCutoffProblem);

analyseSolution(optimalSolution, profileCutoffProblem);

