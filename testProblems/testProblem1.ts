import { profileCutoffProblem } from "../ProfileCuttingStockProblemSolver/interfaces";

export function getTestProblem1() : profileCutoffProblem{
  const profiles = [
  { id: 1, length: 1 },
  { id: 2, length: 2 },
  { id: 3, length: 3 },
  { id: 4, length: 4 },
  { id: 5, length: 5 },
  { id: 6, length: 6 },
  { id: 7, length: 7 },
  { id: 8, length: 8 },
  { id: 9, length: 9 }, 
];

  return {
  profilesCutoffs: profiles,
  profilePieceLength: 10,
  wasteEachCut: 0,
  additionalProfilesLengths: []
};
}
