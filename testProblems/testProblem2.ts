import { profileCutoffProblem } from "../ProfileCuttingStockProblemSolver/interfaces";

export function getTestProblem2(amount = 100, sizeOfMax = 1, additionalProfilesLengths:number[] = []) : profileCutoffProblem{
  const profiles = [];

  for(let i = 0; i < amount; i++){
    profiles.push({id : i, length : Math.round(Math.random() * 60000 * sizeOfMax) / 10})
  }

  return {
  profilesCutoffs: profiles,
  profilePieceLength: 6000,
  wasteEachCut: 2,
  additionalProfilesLengths
};
}
