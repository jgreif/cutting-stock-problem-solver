export interface profileCutoffProblem {
  // Die aufzuteilenden Profilzuschnitte
  profilesCutoffs: profileCutoff[];
  // Länge der Stückware in mm
  profilePieceLength: number;
  // Zusätzlich durch den Sägeprozess weggenommenes Material in mm
  wasteEachCut: number;
  // Längen (in mm) von vorhandenen Restprofilstücken, die neben der Stangenware verwendet werden sollen 
  additionalProfilesLengths: number[];
}

// Profilzuschnitt
export interface profileCutoff {
  length: number;
  id: number;
}

// Zuschnittsverteilung auf Stangenware und Reststücke
export interface profileCutoffSolution {
  profileCuttingsPlans: profileCuttingPlan[];
}

// Zuschnitte, die zu einem Profil zugeordnet sind (Stangeware oder Reststück)
export interface profileCuttingPlan {
  // Zuschnitte für diese Stange / Reststücke
  profileCuts: Set<profileCutoff>;
  // Länge dieser Stange / Reststück in mm
  profileLength: number;
}
