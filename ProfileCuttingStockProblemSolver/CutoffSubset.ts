import { profileCutoff, profileCutoffProblem } from "./interfaces";

/**
 * Ein Sammlung von Profilzuschnitten für eine Profilstange (oder Reststange)
 * Dies ist ein subset aller Zuschnitte
 */
export class CutoffSubset {
  // Alle Zuschnitt in dieser Zuordnung
  profileCutoffs: Set<profileCutoff> = new Set();
  // Länge dieser Stange
  profilePieceLength: number;
  // Weggenommenes Material durch das Sägen
  wasteEachCut: number;

  constructor(profileCutoffProblem: profileCutoffProblem) {
    this.profilePieceLength = profileCutoffProblem.profilePieceLength;
    this.wasteEachCut = profileCutoffProblem.wasteEachCut;
  }

  add(profileCutoff: profileCutoff) {
    this.profileCutoffs.add(profileCutoff);
  }

  remove(profileCutoff: profileCutoff) {
    this.profileCutoffs.delete(profileCutoff);
  }

  amountOfProfiles() {
    return this.profileCutoffs.size;
  }

  /**
   * Gibt die Summer aller Profilzuschnitte wieder (inclusive wegfallendem Material durch die Säge)
   * 
   * @returns {number}
   */
  getAccumulativeLength(): number {
    let length = 0;
    for (const profile of this.profileCutoffs) {
      length += profile.length + this.wasteEachCut;
    }

    return length;
  }

  /**
   * Gibt die verbleibende Restlänge der Stange nach Sägen aller Zuschnitte wieder 
   * (wegfallendes Material durch das Sägen eingerechnet)
   * 
   * @returns {number}
   */
  getRestLength(): number {
    return this.profilePieceLength - this.getAccumulativeLength();
  }
}
