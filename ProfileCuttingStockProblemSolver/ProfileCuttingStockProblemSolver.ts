import { CutoffSubset } from "./CutoffSubset";
import {
  profileCutoff,
  profileCutoffProblem as profileCutoffProblem,
  profileCutoffSolution,
} from "./interfaces";

/**
 * Löst das Zuschnittproblem und berechnet eine möglichst gute Zuschnittverteilung auf die
 * Stangenware unter berücksichtigung von optionalen Restteilen.
 * Restteile werden priorisiert verwendet
 *
 * Lösungsansatz ist ein rekursiver MBS (Minimum Bin Slack), welcher eine gute Lösung des 1D Bin Packings
 * Problems für typische Anwendungsgrößen von Zuschnitten liefert.
 * Implementierung und Wahl des Algorithmus basiert hauptsächlich auf folgendem Paper :
 * https://www.researchgate.net/publication/350365638_One-Dimensional_Bin_Packing_Problem_An_Experimental_Study_of_Instances_Difficulty_and_Algorithms_Performance
 */
export class ProfileCuttingStockProblemSolver {
  /**
   * Löst das Zuschnittproblem und berechnet eine möglichst gute Zuschnittverteilung auf die
   * Stangenware unter berücksichtigung von optionalen Restteilen.
   *
   * PS : Da das 1D Bin Packing Problem NP schwer ist, ist "optimalDistribution" bisschen gelogen ¯\_(ツ)_/¯
   *
   * @param profileCutoffProblem - das zu Lösende Zuschnittsproblem
   * @param additionalProfilesLengths
   * @returns
   */
  static solveCuttingStockProblem(
    profileCutoffProblem: profileCutoffProblem
  ): profileCutoffSolution {
    return this.solve(profileCutoffProblem);
  }

  private static solve(
    profileCutoffProblem: profileCutoffProblem
  ): profileCutoffSolution {
    // copy original Problem to not alter it in process
    let remainingProfileCutoffProblem: profileCutoffProblem = {
      profilesCutoffs: [...profileCutoffProblem.profilesCutoffs],
      profilePieceLength: profileCutoffProblem.profilePieceLength,
      wasteEachCut: profileCutoffProblem.wasteEachCut,
      additionalProfilesLengths: [
        ...profileCutoffProblem.additionalProfilesLengths,
      ].sort((a, b) => a - b),
    };

    const optimalProfileSubsets: profileCutoffSolution = {
      profileCuttingsPlans: [],
    };

    while (
      remainingProfileCutoffProblem.profilesCutoffs.length > 0
    ) {
      // von groß nach klein sortieren
      remainingProfileCutoffProblem.profilesCutoffs.sort(
        (profileCutoffA: profileCutoff, profileCutoffB: profileCutoff) =>
          profileCutoffA.length < profileCutoffB.length ? 1 : -1
      );

      if (remainingProfileCutoffProblem.additionalProfilesLengths.length != 0) {
        const nextProfileLength =
          remainingProfileCutoffProblem.additionalProfilesLengths.shift();
        if (nextProfileLength) {
          remainingProfileCutoffProblem.profilePieceLength = nextProfileLength;
        } else {
          remainingProfileCutoffProblem.profilePieceLength =
            profileCutoffProblem.profilePieceLength;
        }
      } else {
        remainingProfileCutoffProblem.profilePieceLength =
          profileCutoffProblem.profilePieceLength;
      }

      const bestSubset = this.getBestProfileSubset(
        remainingProfileCutoffProblem
      );

      optimalProfileSubsets.profileCuttingsPlans.push({
        profileCuts: bestSubset.profileCutoffs,
        profileLength: remainingProfileCutoffProblem.profilePieceLength,
      });

      // entferne optimales Set aus verbleibenden Profileschnitten
      for (const profile of bestSubset.profileCutoffs) {
        const index =
          remainingProfileCutoffProblem.profilesCutoffs.indexOf(profile);
        if (index === -1) continue;
        remainingProfileCutoffProblem.profilesCutoffs.splice(index, 1);
      }
    }

    return optimalProfileSubsets;
  }

  private static getBestProfileSubset(
    profileCutoffProblem: profileCutoffProblem,
    currentSubset?: CutoffSubset,
    bestSubset?: CutoffSubset,
    q = 0
  ) {
    if (!currentSubset) {
      currentSubset = new CutoffSubset(profileCutoffProblem);
    }
    if (!bestSubset) {
      bestSubset = new CutoffSubset(profileCutoffProblem);
    }

    for (let i = q; i < profileCutoffProblem.profilesCutoffs.length; i++) {
      const potentialProfileCutoff = profileCutoffProblem.profilesCutoffs[i];

      // Es wird immer ein extra Verschnitt benötigt, zwischen zwei Teilen, da die Säge Material wegnimmt
      const neededRestLength =
        potentialProfileCutoff.length + profileCutoffProblem.wasteEachCut;

      if (neededRestLength <= currentSubset.getRestLength()) {
        currentSubset.add(potentialProfileCutoff);

        this.getBestProfileSubset(
          profileCutoffProblem,
          currentSubset,
          bestSubset,
          i + 1
        );

        currentSubset.remove(potentialProfileCutoff);

        // Set ist bereits optimal, kann also nichts hinzugefügt werden
        if (bestSubset.getRestLength() < profileCutoffProblem.wasteEachCut) {
          return bestSubset;
        }
      }
    }

    // Konnte keines der verbleibenden Profilezuschnitte hinzugefügt werden, wird geschaut ob diese Zuordnung von
    // Zuschnitten zu einer Stange besser als die bisher gefundene ist.
    if (currentSubset.getRestLength() < bestSubset.getRestLength()) {
      // Übernehme das Set als das optimale. Dadurch dass currentSubset im weiteren fürs andere Zuordnungen
      // verwendet wird, darf hier nur der Inhalt übernommen werden, aber nicht das Set selbst
      bestSubset.profileCutoffs = new Set([...currentSubset.profileCutoffs]);
    }

    return bestSubset;
  }
}
