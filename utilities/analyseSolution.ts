import { profileCutoffProblem, profileCutoffSolution } from "../ProfileCuttingStockProblemSolver/interfaces";

export function analyseSolution(
  solution: profileCutoffSolution,
  originalProblem: profileCutoffProblem,
): void {
  let accumulativeWaste = 0;
  let accumulativeProfileLength = 0;

  let cutOffLengths = "";
  for (const profileCutoff of originalProblem.profilesCutoffs) {
    cutOffLengths += Math.round(profileCutoff.length * 10) / 10 + "mm, ";
  }
  console.log("Profilschnitte der Länge " + cutOffLengths);

  const neededProfilsPieces = solution.profileCuttingsPlans.length - (originalProblem.additionalProfilesLengths.length)
  console.log(`\nEs werden ${neededProfilsPieces} Stangen benötigt.`);

  for (let i = 0; i < solution.profileCuttingsPlans.length; i++) {
    const cuttingPlan = solution.profileCuttingsPlans[i];
    const profileCuts = cuttingPlan.profileCuts;
    let usedLength = 0;
    for (const profile of profileCuts) {
      usedLength += profile.length;
    }
    // add cut Waste
    usedLength += (profileCuts.size - 1) * originalProblem.wasteEachCut;
    const waste =
      Math.round((cuttingPlan.profileLength - usedLength) * 10) / 10;

    console.log(
      `Profil ${i+1} mit der Länge ${cuttingPlan.profileLength} wird gesägt in :`
    );
    for (const profileCut of profileCuts) {
      console.log(`     ${profileCut.length}mm`);
    }



    if (i !== solution.profileCuttingsPlans.length - 1) {
      console.log(`     Ergibt ${waste}mm Verschnitt`);
      accumulativeWaste += waste;
      accumulativeProfileLength += cuttingPlan.profileLength;
    } else {
      console.log(`     Ergibt ein Reststück der Länge ${waste}mm.`);
      accumulativeWaste = Math.round(accumulativeWaste * 10) / 10;
      const wasteInPercent =
        Math.round((accumulativeWaste / accumulativeProfileLength) * 1000) / 10;
      console.log(
        `Dies macht in Summe einen Verschnitt von ${accumulativeWaste}mm, welches ${wasteInPercent}% der verwendeten Stangen entspricht`
      );
    }
  }
}
